const express = require('express')
const app = express()
const path = require("path")

//raw data for switching Route
fields = [{name: "Technology"}, {name: "News"}, {name: "Sports"}, {name: "Travel"}];

//Declaring EJS As render Engine
app.set('view engine',"ejs")

//Setting path for view
app.set("views",path.join(__dirname,"views"))

//serving index page on default route
app.use("/",(req,res)=>{
    res.render("index")
})

app.listen(3000)